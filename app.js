require('dotenv').config();

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cors = require('cors');
var Song = require('./models/song');
var Artist = require('./models/artist');
var SongsCollection = require('./collections/song');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(cors());

app.get('/api/songs', function(request, response) {
  Song.fetchAll().then(function(songs) {
    response.json(songs);
  });

  // eager load
  // new SongsCollection().fetch({ withRelated: ['artist'] }).then(function(songs) {
  //   response.json(songs);
  // });
});

// lazy load
// app.get('/api/songs', function(request, response) {
//   Song
//     .fetchAll()
//     .then(function(songs) {
//       var artists = songs.map(function(song) {
//         return song.related('artist').fetch();
//       });

//       return Promise.all(artists);
//     })
//     .then(function(artists) {
//       response.json(artists);
//     });
// });

app.get('/api/songs/:id', function(request, response) {
  Song
    .where('id', request.params.id)
    .fetch({ 
      require: true,
      withRelated: ['artist']
    })
    .then(function(song) {
      response.json(song);
    }, function(e) {
      response.status(404).json({
        error: {
          message: 'Song not found'
        }
      });
    });
});

app.post('/api/artists', function(request, response) {
  var artist = new Artist({
    artist_name: request.body.name
  });

  artist.save().then(function() {
    response.json(artist);
  });
});

app.delete('/api/artists/:id', function(request, response) {
  var artist = new Artist({
    id: request.params.id
  });

  artist
    .destroy({ require: true })
    .then(function(artist) {
      response.json(artist);
    }, function() {
      response.status(404).json({
        error: 'song not found'
      });
    });
});

app.put('/api/artists/:id', function(request, response) {
  Artist
    .where('id', request.params.id)
    .fetch({ require: true })
    .then(function(artist) {
      artist.set('artist_name', request.body.name);
      return artist.save();
    }, function(e) {
      response.status(404).json({
        error: {
          message: 'artist not found'
        }
      });
    })
    .then(function(artist) {
      response.json(artist);
    });
});

app.listen(8000);
